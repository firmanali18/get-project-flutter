[![flutter platform](https://img.shields.io/badge/Platform-Flutter-yellow.svg)](https://flutter.io)

# Get Studi Project

A Flutter project With GetX Framework.

## List GetX Features

List of GetX Features Used In This Project Are:

- [x] State Management
- [x] Route management
- [x] Dependency management
- [x] (Utils) Translations & Locales
- [x] (Utils) Change Theme
- [x] (Utils) GetConnect
- [x] (Utils) Get Snack Bar
- [x] (Utils) Get Dialog
- [x] (Utils) Get Bottom Sheets
- [x] Worker

## Get Started

You can find this framework [GetX](https://pub.dev/packages/get#state-management) for more information