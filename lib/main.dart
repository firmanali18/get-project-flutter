import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:get_studi_project/app/constant/style.dart';
import 'package:get_studi_project/app/utils/translations.dart';

import 'app/routes/app_pages.dart';

Future<void> main() async {
  await GetStorage.init();
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final c = Get.put(MyController());
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: c.theme,
      title: "Application",
      translations: Messages(),
      locale: const Locale('en', 'US'),
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
    );
  }
}

class MyController extends GetxController {
  final _theme = ThemesData().light.obs;
  set theme(value) => _theme.value = value;
  get theme => _theme.value;

  @override
  void onInit() {
    final box = GetStorage();
    final dataTheme = box.read('theme');
    if (dataTheme == 'dark') {
      theme = ThemesData().dark;
    } else {
      theme = ThemesData().light;
    }
    super.onInit();
  }
}
