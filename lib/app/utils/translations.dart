import 'package:get/get.dart';

class Messages extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'en_US': {
          'count_button': 'Get State Management Counter',
          'fav_button': 'Get Life Cycle Aplication',
          'search_button': 'Get Worker Aplication',
          'item_button': 'Get Dynamic URL Navigation',
          'snack_bar_button': 'Show Snack Bar',
          'dialog_button': 'Show Dialog',
          'bottom_sheets_button': 'Show Bottom Sheets',
          'translations_id_button': 'Get Translations to ID',
          'translations_en_button': 'Get Translations to EN',
          'themes_button': 'Get Change Themes',
          'get_connect_button': 'Get GetConnect API',
        },
        'id_ID': {
          'count_button': 'Get Counter State Management',
          'fav_button': 'Get Aplikasi Life Cycle',
          'search_button': 'Get Aplikasi Worker',
          'item_button': 'Get Navigasi Dynamic URL',
          'snack_bar_button': 'Tampilkan Snack Bar',
          'dialog_button': 'Tampilkan Dialog',
          'bottom_sheets_button': 'Tampilkan Bottom Sheets',
          'translations_id_button': 'Get Translations ke ID',
          'translations_en_button': 'Get Translations ke EN',
          'themes_button': 'Get Ganti Tema',
          'get_connect_button': 'Get GetConnect API',
        }
      };
}
