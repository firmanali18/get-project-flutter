import 'package:flutter/material.dart';

class ThemesData {
  final light = ThemeData.light().copyWith(
      elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
    fixedSize: const Size(240, 50),
  )));

  final dark = ThemeData.dark().copyWith(
      elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
    fixedSize: const Size(240, 50),
    primary: Colors.grey,
  )));
}
