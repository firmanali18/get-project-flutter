import 'package:get/get.dart';
import 'package:get_studi_project/app/modules/count/views/count_view.dart';
import 'package:get_studi_project/app/modules/get_item/views/item_view.dart';
import 'package:get_studi_project/app/modules/home/bindings/home_binding.dart';
import 'package:get_studi_project/app/modules/search/bindings/search_binding.dart';
import 'package:get_studi_project/app/modules/search/views/search_view.dart';

import '../modules/count/binding/count_binding.dart';
import '../modules/fact/bindings/fact_binding.dart';
import '../modules/fact/presentation/views/fact_view.dart';
import '../modules/fav_number/bindings/fav_number_binding.dart';
import '../modules/fav_number/views/fav_number_view.dart';
import '../modules/get_item/views/detail_view.dart';
import '../modules/home/home_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.COUNT,
      page: () => CountView(),
      binding: CountBinding(),
    ),
    GetPage(
      name: _Paths.FAV,
      page: () => FavNumberView(),
      binding: FavNumberBinding(),
    ),
    GetPage(
      name: _Paths.SEARCH,
      page: () => SearchView(),
      binding: SearchBinding(),
    ),
    GetPage(
      name: _Paths.ITEM,
      page: () => GetItemView(),
    ),
    GetPage(
      name: _Paths.DETAIL + '/:id',
      page: () => DetailView(),
    ),
    GetPage(
      name: _Paths.FACT,
      page: () => FactView(),
      binding: FactBinding(),
    ),
  ];
}
