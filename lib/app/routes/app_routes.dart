part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const COUNT = _Paths.COUNT;
  static const FAV = _Paths.FAV;
  static const SEARCH = _Paths.SEARCH;
  static const ITEM = _Paths.ITEM;
  static const DETAIL = _Paths.DETAIL;
  static const FACT = _Paths.FACT;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const COUNT = '/count';
  static const FAV = '/fav';
  static const SEARCH = '/search';
  static const ITEM = '/item';
  static const DETAIL = '/detail';
  static const FACT = '/fact';
}
