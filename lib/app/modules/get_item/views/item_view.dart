import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_studi_project/app/routes/app_pages.dart';

class GetItemView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('ListItemView'),
        centerTitle: true,
      ),
      body: Center(
          child: ElevatedButton(
        onPressed: () => Get.toNamed(Routes.DETAIL + '/1'),
        child: const Text('Item 1'),
      )),
    );
  }
}
