import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DetailView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('DetailItemView'),
        centerTitle: true,
      ),
      body: Center(
        child: Text('Item Produk ke - ${Get.parameters['id']}'),
      ),
    );
  }
}
