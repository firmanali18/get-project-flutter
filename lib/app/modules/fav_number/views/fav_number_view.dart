import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controllers/fav_number_controller.dart';

class FavNumberView extends GetView<FavNumberController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Favorite Number View'),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text('Input Number :'),
            GetBuilder<FavNumberController>(
              initState: (state) {
                controller.initNumber();
              },
              builder: (_) {
                return Text(controller.num.toString());
              },
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
              child: TextField(
                controller: controller.textC,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Input Favorite Number',
                ),
              ),
            ),
            ElevatedButton(
              onPressed: () => controller.addNumber(),
              child: const Text('Add Number'),
            )
          ],
        ),
      ),
    );
  }
}
