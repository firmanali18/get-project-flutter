import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class FavNumberController extends GetxController {
  late TextEditingController textC;
  var num;

  void addNumber() {
    num = textC.text;
    update();
  }

  void initNumber() {
    num = 0;
    update();
  }

  @override
  void onInit() {
    textC = TextEditingController();
    super.onInit();
  }

  @override
  void onClose() {
    textC.dispose();
    super.onClose();
  }
}
