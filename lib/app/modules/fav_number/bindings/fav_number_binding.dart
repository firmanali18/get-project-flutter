import 'package:get/get.dart';

import '../controllers/fav_number_controller.dart';

class FavNumberBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<FavNumberController>(
      () => FavNumberController(),
    );
  }
}
