import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:get_studi_project/app/constant/style.dart';

class HomeController extends GetxController {
  void getSnackBar() => Get.snackbar(
        'Test Snack Bar',
        'This Testing Snack Bar With GetX',
        backgroundColor: Colors.white,
      );

  void getDialog() => Get.defaultDialog(
        title: 'Test Dialog',
        middleText: 'This Test Default Dialog With GetX',
        textConfirm: 'Okay',
        textCancel: 'Close',
      );

  void getBottomSheet() => Get.bottomSheet(
        Container(
          height: 100,
          alignment: Alignment.center,
          child: const Text('Test Open Bottom Sheet'),
        ),
        backgroundColor: Colors.white,
      );

  void chageTheme() {
    final box = GetStorage();
    if (Get.isDarkMode) {
      box.write('theme', 'light');
    } else {
      box.write('theme', 'dark');
    }

    Get.changeTheme(
      Get.isDarkMode ? ThemesData().light : ThemesData().dark,
    );
  }
}
