import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_studi_project/app/modules/home/controllers/home_controller.dart';
import 'package:get_studi_project/app/routes/app_pages.dart';

import '../../constant/style.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home View'),
        centerTitle: true,
      ),
      body: Center(
          child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
                padding: const EdgeInsets.all(20.0),
                child: const Text(
                  'GetX Feature List',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                )),
            ElevatedButton(
              onPressed: () => Get.toNamed(Routes.COUNT),
              child: Text('count_button'.tr),
            ),
            const SizedBox(height: 10.0),
            ElevatedButton(
              onPressed: () => Get.toNamed(Routes.FAV),
              child: Text('fav_button'.tr),
            ),
            const SizedBox(height: 10.0),
            ElevatedButton(
              onPressed: () => Get.toNamed(Routes.SEARCH),
              child: Text('search_button'.tr),
            ),
            const SizedBox(height: 10.0),
            ElevatedButton(
              onPressed: () => Get.toNamed(Routes.ITEM),
              child: Text('item_button'.tr),
            ),
            const SizedBox(height: 10.0),
            ElevatedButton(
              onPressed: () => controller.getSnackBar(),
              child: Text('snack_bar_button'.tr),
            ),
            const SizedBox(height: 10.0),
            ElevatedButton(
              onPressed: () => controller.getDialog(),
              child: Text('dialog_button'.tr),
            ),
            const SizedBox(height: 10.0),
            ElevatedButton(
              onPressed: () => controller.getBottomSheet(),
              child: Text('bottom_sheets_button'.tr),
            ),
            const SizedBox(height: 10.0),
            ElevatedButton(
              onPressed: () => Get.updateLocale(const Locale('id', 'ID')),
              child: Text('translations_id_button'.tr),
            ),
            const SizedBox(height: 10.0),
            ElevatedButton(
              onPressed: () => Get.updateLocale(const Locale('en', 'US')),
              child: Text('translations_en_button'.tr),
            ),
            const SizedBox(height: 10.0),
            ElevatedButton(
              onPressed: () => controller.chageTheme(),
              child: Text('themes_button'.tr),
            ),
            const SizedBox(height: 10.0),
            ElevatedButton(
              onPressed: () => Get.toNamed(Routes.FACT),
              child: Text('get_connect_button'.tr),
            ),
          ],
        ),
      )),
    );
  }
}
