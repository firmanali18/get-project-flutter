import '../entity/fact_model.dart';

abstract class IFactRepository {
  Future<FactModel> getFact();
}
