import 'dart:convert';

FactModel factFromJson(String str) => FactModel.fromJson(json.decode(str));

String factToJson(FactModel data) => json.encode(data.toJson());

class FactModel {
  FactModel({
    required this.fact,
    required this.length,
  });

  final String fact;
  final int length;

  factory FactModel.fromJson(Map<String, dynamic> json) => FactModel(
        fact: json["fact"],
        length: json["length"],
      );

  Map<String, dynamic> toJson() => {
        "fact": fact,
        "length": length,
      };
}
