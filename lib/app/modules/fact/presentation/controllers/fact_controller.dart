import 'package:get/get.dart';
import 'package:get_studi_project/app/modules/fact/data/fact_repository.dart';

class FactController extends GetxController {
  FactController({required this.repository});

  FactRepository repository;

  var fact = ''.obs;

  void getFact() {
    repository
        .getFact()
        .then((value) => fact.value = value.fact)
        .onError((error, _) => fact.value = 'Error Get Data From API');
  }
}
