import 'package:get/get.dart';
import 'package:get_studi_project/app/modules/fact/data/api/fact_api_provider.dart';
import 'package:get_studi_project/app/modules/fact/data/fact_repository.dart';

import '../presentation/controllers/fact_controller.dart';

class FactBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<FactController>(
      () => FactController(
        repository: FactRepository(provider: FactProvider()),
      ),
    );
  }
}
