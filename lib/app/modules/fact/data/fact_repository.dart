import 'package:get_studi_project/app/modules/fact/data/api/fact_api_provider.dart';
import 'package:get_studi_project/app/modules/fact/domain/entity/fact_model.dart';

import '../domain/adapter/repository_adapter.dart';

class FactRepository implements IFactRepository {
  FactRepository({required this.provider});
  final FactProvider provider;

  @override
  Future<FactModel> getFact() async => await provider.getFact("/fact");
}
