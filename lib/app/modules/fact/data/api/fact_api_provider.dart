import 'package:get/get.dart';
import 'package:get_studi_project/app/modules/fact/domain/entity/fact_model.dart';

abstract class IFactProvider {
  Future<FactModel> getFact(String path);
}

class FactProvider extends GetConnect implements IFactProvider {
  final API_URL = 'https://catfact.ninja';

  @override
  Future<FactModel> getFact(String path) async {
    final query = await get(API_URL + path);
    return FactModel.fromJson(query.body);
  }
}
