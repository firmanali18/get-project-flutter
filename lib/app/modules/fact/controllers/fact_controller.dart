import 'package:get/get.dart';
import 'package:get_studi_project/app/data/api/fact.dart';

class FactController extends GetxController {
  var fact = ''.obs;

  void getQuotes() {
    FactProvider().getQuotes().then((value) => fact.value = value.body['fact']);
  }
}
