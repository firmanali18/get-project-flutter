import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SearchController extends GetxController {
  late TextEditingController textC;
  var query = ''.obs;
  var searchHint = [
    'Aceh',
    'Bali',
    'Cibubur',
    'Demak',
    'Jakarta',
    'Kudus',
    'Lombok',
    'Mataram',
    'NTT',
    'Palu',
    'Sulawesi',
    'Tulung Agung'
  ];
  var searchResult = [].obs;

  void onChange() => query.value = textC.text;

  void onResult() {
    searchResult.clear();
    for (var e in searchHint) {
      if (e.toLowerCase().contains(query.value.toLowerCase())) {
        searchResult.add(e);
      }
    }
  }

  @override
  void onInit() {
    textC = TextEditingController();
    super.onInit();
  }

  @override
  void onClose() {
    textC.dispose();
    super.onClose();
  }

  @override
  void onReady() {
    debounce(query, (_) => onResult(), time: const Duration(seconds: 2));
    super.onReady();
  }
}
