import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controllers/search_controller.dart';

class SearchView extends GetView<SearchController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Search View'),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
              child: TextField(
                onChanged: (_) => controller.onChange(),
                controller: controller.textC,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Input Search Data',
                ),
              ),
            ),
            const Text(
              'Search Result',
              style: TextStyle(fontSize: 20),
            ),
            Expanded(
              child: Obx(
                () => ListView.builder(
                  padding: const EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 20.0),
                  itemCount: controller.searchResult.length,
                  itemBuilder: (context, index) => ListTile(
                    title: Text(
                      controller.searchResult[index],
                      style: const TextStyle(fontSize: 18),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
